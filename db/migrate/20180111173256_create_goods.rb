class CreateGoods < ActiveRecord::Migration[5.1]
  def change
    create_table :goods do |t|
      t.string :name

      t.timestamps
    end
    add_index :goods, :name, unique: true
  end
end
