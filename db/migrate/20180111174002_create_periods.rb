class CreatePeriods < ActiveRecord::Migration[5.1]
  def change
    create_table :periods do |t|
      t.references :good, foreign_key: true
      t.date :date
      t.decimal :revenue, precision: 10, scale: 2

      t.timestamps
    end
  end
end
