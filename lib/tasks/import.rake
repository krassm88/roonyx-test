namespace :import do
  task :goods do
    xlsx = Roo::Spreadsheet.open("#{Rails.root}/db/data.xlsx")

    # Определяем лист файла
    sheet = xlsx.sheet(0)

    # Количество строк и колонок
    row_count = sheet.last_row
    column_count = sheet.last_column

    # Номера строк и колонок в xlsx файлах начинаются с 1
    (row_count+1).times do |row_number|
      next if row_number == 1
      good_name = sheet.cell(row_number, 1)

      good = Good.create(name: good_name)

      if good.persisted?
        puts good.name
        draw_line

        (column_count+1).times do |column_number|
          next if column_number == 1

          period = good.periods.create(
            date: sheet.cell(1, column_number),
            revenue: sheet.cell(row_number, column_number)
          )

          puts "#{period.date} - #{period.revenue} - created" if period.persisted?
        end

        draw_line
      end

      def draw_line
        line = []
        15.times { line << '-----' }
        puts line.join
      end
    end
  end
end
