class SalesController < ApplicationController
  before_action :check_date_params, only: :index

  def index
    respond_to do |format|
      if @errors.any?
        format.json { render json: { errors: @errors }, status: 422 }
      else
        @goods = Good.includes(:periods)
        format.json
      end

      format.html
      format.js
    end
  end

  private

  def check_date_params
    @errors = { }

    %i[from to].each do |attr|
      @errors[attr.to_sym] = "Неверный формат #{attr}" unless correct_format?(params[attr])
    end

    if @errors.empty?
      @errors[:from_to] = "From не может быть больше TO" if Date.parse(params[:from]) > Date.parse(params[:to])
    end
  end

  def correct_format?(date)
    begin
      Date.parse(date)
    rescue
      return false
    end
    true
  end
end
