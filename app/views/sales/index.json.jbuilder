json.set! :from, params[:from]
json.set! :to, params[:to]

json.set! :goods do
  json.array! @goods do |good|
    json.title good.name
    json.revenue good.periods.during_period_revenue_sum(params[:from], params[:to])
  end
end