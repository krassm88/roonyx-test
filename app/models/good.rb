class Good < ApplicationRecord
  has_many :periods, dependent: :destroy

  validates :name, presence: true, uniqueness: true

  def name=(value)
    write_attribute(:name, value.downcase) if value
  end
end
