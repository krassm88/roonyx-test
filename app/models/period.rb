class Period < ApplicationRecord
  scope :during_period_revenue_sum, -> (from, to) { where('date BETWEEN ? AND ?', Date.parse(from), Date.parse(to)).sum(:revenue) }

  belongs_to :good

  validates :date, presence: true, timeliness: { type: :date }
  validates :revenue, presence: true, numericality: true

  def revenue
    self[:revenue].to_f
  end
end
